# demo-test

a test module in iOS

## 测试模块

使用原生编写的测试模块，测试`axe`的主要功能。

## 是镜像仓库

注意，本仓库是  [demo-login](https://coding.net/u/axe-org/p/demo-test/git?public=true) 的镜像。 由于`github`的`lfs`访问问题，墙内访问`lfs`实在困难， 所以将实际代码托管在 `coding.net`上。

本仓库的内容会与实际仓库有一些差异。