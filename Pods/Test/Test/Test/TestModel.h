//
//  TestModel.h
//  Test
//
//  Created by 罗贤明 on 2018/5/18.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasicModel.h"

@interface TestModel : BasicModel
@property (nonatomic,strong) NSString *string;
@property (nonatomic,strong) NSNumber *number;
@property (nonatomic,strong) NSDictionary *map;
@property (nonatomic,strong) NSArray *list;
@end
