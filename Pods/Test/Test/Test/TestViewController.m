//
//  TestViewController.m
//  Demo
//
//  Created by 罗贤明 on 2018/5/12.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "TestViewController.h"
#import "Axe.h"
#import "DemoGround.h"
#import "MBProgressHUD.h"
#import "AXEWebViewController.h"
#import "TestModel.h"
#import "API.h"
#import <Login/API.h>
#import <Test/API.h>
@interface IntrinsicContentSizeView : UIView

@property (nonatomic,strong) UIView *view;

@end

@implementation IntrinsicContentSizeView

- (instancetype)initWithView:(UIView *)view {
    if (self = [super init]) {
        _view = view;
        [self addSubview:view];
    }
    return self;
}

- (CGSize)intrinsicContentSize {
    return _view.frame.size;
}

@end

// jsmodel.
@protocol LoginUserInfo <NSObject>

@property (nonatomic,strong) NSString *account;
@property (nonatomic,strong) NSNumber *level;
@property (nonatomic,strong) NSDictionary *detailInfo;
@property (nonatomic,strong) NSArray *tagList;
@end




typedef void (^TableViewItemClickBlock)(void);

@interface TestTableViewItem : NSObject

@property (nonatomic,strong) NSString *title;
@property (nonatomic,copy) TableViewItemClickBlock block;

@end

@implementation TestTableViewItem

@end

@interface TestViewController () <UITableViewDelegate,UITableViewDataSource,UINavigationControllerDelegate, UIImagePickerControllerDelegate>

@property (nonatomic,strong) UITableView *tableView;

@property (nonatomic,strong) NSMutableArray<TestTableViewItem *> *cells;

@property (nonatomic,strong) id<AXEListenerDisposable> eventAListener;
@property (nonatomic,strong) id<AXEListenerDisposable> loginListener;

@end

@implementation TestViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    _cells = [[NSMutableArray alloc] init];
    //    self.title = @"test";
    // 如果设置title ,会重置 tabbarItem的title...
    self.navigationItem.title = @"Test";
    
    _tableView = [[UITableView alloc] initWithFrame:self.view.bounds style:UITableViewStylePlain];
    [self.view addSubview:_tableView];
    _tableView.backgroundColor = [UIColor whiteColor];
    _tableView.delegate = self;
    _tableView.dataSource = self;
    _tableView.estimatedSectionHeaderHeight = 0;
    _tableView.estimatedSectionFooterHeight = 0;
    // 这里添加内容
    __weak TestViewController *wself = self;
    
    // 路由跳转测试
    [self addCellTitle:@"login" action:^{
        [[AXERouter sharedRouter] jumpTo:LOGIN_ROUTER_LOGIN fromViewController:wself];
    }];
    [self addCellTitle:@"register" action:^{
        [wself jumpTo:LOGIN_ROUTER_REGISTER];
    }];
    // 回调
    [self addCellTitle:@"login with callback" action:^{
        [wself jumpTo:LOGIN_ROUTER_LOGIN withParams:nil finishBlock:^(AXEData *payload) {
            id<LoginUserInfo> userInfo = [payload modelForKey:LOGIN_DATA_USERINFO];

            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:wself.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.detailsLabel.text = [NSString stringWithFormat:@"登录成功 ,信息如下 ：\n 帐号 : %@ \n 等级: %@ \n detailInfo: %@ \n tagList: %@", userInfo.account, userInfo.level, userInfo.detailInfo, userInfo.tagList];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
        }];
    }];
    [self addCellTitle:@"register with callback" action:^{
        [wself jumpTo:LOGIN_ROUTER_REGISTER withParams:nil finishBlock:^(AXEData *payload) {
            id<LoginUserInfo> userInfo = [payload modelForKey:LOGIN_DATA_USERINFO];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:wself.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.detailsLabel.text = [NSString stringWithFormat:@"注册成功 ,信息如下 ：\n 帐号 : %@ \n 等级: %@ \n detailInfo: %@ \n tagList: %@", userInfo.account, userInfo.level, userInfo.detailInfo, userInfo.tagList];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
        }];
    }];
    // 传参
    [self addCellTitle:@"login(123455678)" action:^{
        AXEData *data = [AXEData dataForTransmission];
        [data setData:@"12345678" forKey:LOGIN_DATA_ACCOUNT];
        [[AXERouter sharedRouter] jumpTo:LOGIN_ROUTER_LOGIN fromViewController:wself withParams:data finishBlock:^(AXEData *payload) {
            id<LoginUserInfo> userInfo = [payload modelForKey:LOGIN_DATA_USERINFO];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:wself.view animated:YES];
            hud.mode = MBProgressHUDModeText;
            hud.detailsLabel.text = [NSString stringWithFormat:@"登录成功 ,信息如下 ：\n 帐号 : %@ \n 等级: %@ \n detailInfo: %@ \n tagList: %@", userInfo.account, userInfo.level, userInfo.detailInfo, userInfo.tagList];
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
        }];
    }];
    [self addCellTitle:@"" action:^{
        
    }];
    
    // 数据测试
    [self addCellTitle:@"setImage" action:^{
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = wself;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [wself presentViewController:picker animated:YES completion:nil];
    }];
    [self addCellTitle:@"getImage" action:^{
        UIImage *image = [[AXEData sharedData] imageForKey:TEST_DATA_IMAGE];
        if (image) {
            UIImageView *view = [[UIImageView alloc] initWithImage:image];
            view.frame = CGRectMake(0, 0, 300, 300);
            IntrinsicContentSizeView *wrapper = [[IntrinsicContentSizeView alloc] initWithView:view];
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:wself.view animated:YES];
            hud.mode = MBProgressHUDModeCustomView;
            hud.customView = wrapper;
            dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                [hud hideAnimated:YES];
            });
        } else {
            [wself toastMessage:@"当前未设置 key为 image 的图片！！！"];
        }
    }];
    [self addCellTitle:@"setBasicData" action:^{
        // 基础数据类型
        [[AXEData sharedData] setData:@1 forKey:TEST_DATA_NUMBER];
        [[AXEData sharedData] setBool:YES forKey:TEST_DATA_BOOLEAN];
        [[AXEData sharedData] setData:@"ios-string" forKey:TEST_DATA_STRING];
        [[AXEData sharedData] setData:@[@"i",@"o",@"s"] forKey:TEST_DATA_ARRAY];
        [[AXEData sharedData] setData:@{@"type":@"ios",@"hello":@"world"} forKey:TEST_DATA_OBJECT];
        [wself toastMessage:@"设置基础数据！！！"];
    }];
    [self addCellTitle:@"getBasicData" action:^{
        // 基础数据类型
        NSMutableString *message = [[NSMutableString alloc] init];
        [message appendFormat:@"number : %@ \n", [[AXEData sharedData] numberForKey:TEST_DATA_NUMBER]];
        [message appendFormat:@"boolean : %@ \n", [[AXEData sharedData] boolForKey:TEST_DATA_BOOLEAN] ? @"YES" : @"NO"];
        [message appendFormat:@"string : %@ \n", [[AXEData sharedData] stringForKey:TEST_DATA_STRING]];
        [message appendFormat:@"array : %@ \n", [[AXEData sharedData] arrayForKey:TEST_DATA_ARRAY]];
        [message appendFormat:@"object : %@ ", [[AXEData sharedData] dictionaryForKey:TEST_DATA_OBJECT]];
        [wself toastMessage:message];
    }];
    [self addCellTitle:@"setModel" action:^{
        TestModel *model = [[TestModel alloc] init];
        model.string = @"ios";
        model.number = @1;
        model.list = @[@1,@"2"];
        [[AXEData sharedData] setData:model forKey:TEST_DATA_MODEL];
        [wself toastMessage:@"设置 model 类型！"];
    }];
    [self addCellTitle:@"getModel" action:^{
        id<TestModelInterface> model = [[AXEData sharedData] modelForKey:TEST_DATA_MODEL];
        if (model) {
            NSMutableString *message = [[NSMutableString alloc] init];
            [message appendFormat:@"string : %@ \n", model.string];
            [message appendFormat:@"number : %@ \n", model.number];
            [message appendFormat:@"map : %@ \n", model.map];
            [message appendFormat:@"list : %@ ", model.list];
            [wself toastMessage:message];
        } else {
            [wself toastMessage:@"当前未设置 相应键值的model类型 ！"];
        }
    }];
    [self addCellTitle:@"changeModel" action:^{
        // 原生可以直接修改model, 而在js中， 必须进行提交才能修改
        // TODO 后续考虑保持原生与js的表现一致，使用不变类型，即通过data传递和获取的都是不可变的复制对象。
        id<TestModelInterface> model = [[AXEData sharedData] modelForKey:TEST_DATA_MODEL];
        if (model) {
            model.number = @(rand());
            model.string = [NSUUID UUID].UUIDString;
            [wself toastMessage:@"完成对model的修改 ！！"];
        } else {
            [wself toastMessage:@"当前未设置 相应键值的model类型 ！"];
        }
    }];
    [self addCellTitle:@"setDate" action:^{
        [[AXEData sharedData] setData:[NSDate date] forKey:TEST_DATA_DATE];
        [wself toastMessage:@"设置当前时间！"];
    }];
    [self addCellTitle:@"getDate" action:^{
        NSDate *date = [[AXEData sharedData] dateForKey:TEST_DATA_DATE];
        [wself toastMessage: [NSString stringWithFormat:@"date : %@", date]];
    }];
    [self addCellTitle:@"remove('date')" action:^{
        [[AXEData sharedData] removeDataForKey:TEST_DATA_DATE];
        [wself toastMessage:@"成功删除 key 为 date 的数据 ！"];
    }];
    
    // 事件通知测试。
    [self addCellTitle:@"" action:^{
    }];
    [self addCellTitle:@"reigsterEventA" action:^{
        if (wself.eventAListener) {
            [wself toastMessage:@"已注册监听！！！"];
        } else {
            // 事件通知，要注意引用问题。
            wself.eventAListener = [wself registerUIEvent:TEST_EVENT_EVENTA withHandler:^(AXEData *payload) {
                [wself toastMessage:@"eventA !!!!"];
            }];
            [wself toastMessage:@"成功注册监听！！"];
        }
    }];
    
    [self addCellTitle:@"removeEventA" action:^{
        if (wself.eventAListener) {
            [wself.eventAListener dispose];
            wself.eventAListener = nil;
            [wself toastMessage:@"成功注销！"];
        } else {
            [wself toastMessage:@"并未注册 ！"];
        }
    }];
    [self addCellTitle:@"postEventA" action:^{
        [AXEEvent postEventName:TEST_EVENT_EVENTA];
    }];
    [self addCellTitle:@"listenToLoginStatus" action:^{
        if (wself.loginListener) {
            [wself toastMessage:@"已注册通知！"];
        } else {
            wself.loginListener = [wself registerUIEvent:TEST_EVENT_LOGINSTATUS withHandler:^(AXEData *payload) {
                BOOL login = [payload boolForKey:LOGIN_DATA_LOGIN];
                if (!login) {
                    [wself toastMessage:@"退出登录 ！！！"];
                } else {
                    id<LoginUserInfo> userInfo = [payload modelForKey:LOGIN_DATA_USERINFO];
                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:wself.view animated:YES];
                    hud.mode = MBProgressHUDModeText;
                    hud.offset = CGPointMake(0.f, MBProgressMaxOffset);
                    hud.detailsLabel.text = [NSString stringWithFormat:@"登录成功 ,信息如下 ：\n 帐号 : %@ \n 等级: %@ \n detailInfo: %@ \n tagList: %@", userInfo.account, userInfo.level, userInfo.detailInfo, userInfo.tagList];
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
                        [hud hideAnimated:YES];
                    });
                }
            }];
        }
    }];
    [self addCellTitle:@"removeLoginListener" action:^{
        if (wself.loginListener) {
            [wself.loginListener dispose];
            wself.loginListener = nil;
            [wself toastMessage:@"成功注销！"];
        } else {
            [wself toastMessage:@"并未注册 ！"];
        }
    }];
    [self addCellTitle:@"logOut" action:^{
        [[AXEData sharedData] removeDataForKey:LOGIN_DATA_USERINFO];
        AXEData *data = [AXEData dataForTransmission];
        [data setBool:NO forKey:LOGIN_DATA_LOGIN];
        [AXEEvent postEventName:TEST_EVENT_LOGINSTATUS withPayload:data];
    }];
    [self addCellTitle:@"" action:^{
    }];
    [self addCellTitle:@"home/react" action:^{
        [wself jumpTo:@"axe://home/react"];
    }];
    
    [_tableView reloadData];
}

- (void)toastMessage:(NSString *)massage {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabel.text = massage;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });
}

- (void)addCellTitle:(NSString *)title action:(TableViewItemClickBlock)block {
    TestTableViewItem *item = [[TestTableViewItem alloc] init];
    item.title = title;
    item.block = block;
    [_cells addObject:item];
}

//UIImagePickerControllerDelegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary<NSString *,id> *)info {
    UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
    [picker dismissViewControllerAnimated:YES completion:^{
        [[AXEData sharedData] setData:image forKey:TEST_DATA_IMAGE];
        UIImageView *view = [[UIImageView alloc] initWithImage:image];
        view.frame = CGRectMake(0, 0, 300, 300);
        IntrinsicContentSizeView *wrapper = [[IntrinsicContentSizeView alloc] initWithView:view];
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        hud.mode = MBProgressHUDModeCustomView;
        hud.customView = wrapper;
        hud.label.text = @"成功设置图片！！";
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(3 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            [hud hideAnimated:YES];
        });
    }];
}

#pragma mark - UITableViewDataSource & UITableViewDelegate
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _cells.count;
}
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 15;
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section {
    return 10;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    static NSString *cellIdentifier = @"cellIdentifier";
    cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    if (!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:17];
        cell.textLabel.textAlignment = NSTextAlignmentCenter;
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    cell.textLabel.text = _cells[indexPath.row].title;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (_cells[indexPath.row].block) {
        _cells[indexPath.row].block();
    }
}


@end
