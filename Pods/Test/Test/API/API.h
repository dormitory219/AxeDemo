//
//  API.h
//  Login
//
//  Created by 罗贤明 on 2018/4/15.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import <Foundation/Foundation.h>
// API 文件， 用于定义模块的对外接口。命名规范:
// MODULE[_PAGE]_TYPE[_item]

#pragma mark - data
/**
   测试 model
 */
@protocol TestModelInterface <NSObject>
@property (nonatomic,strong) NSString *string;
@property (nonatomic,strong) NSNumber *number;
@property (nonatomic,strong) NSDictionary *map;
@property (nonatomic,strong) NSArray *list;
@end


#define TEST_DATA_IMAGE                 @"image"

#define TEST_DATA_NUMBER                @"number"
#define TEST_DATA_BOOLEAN               @"boolean"
#define TEST_DATA_STRING                @"string"
#define TEST_DATA_ARRAY                 @"array"
#define TEST_DATA_OBJECT                @"object"
#define TEST_DATA_MODEL                 @"model"
#define TEST_DATA_DATE                  @"date"

#define TEST_DATA_BOOLEAN               @"boolean"
#define TEST_DATA_BOOLEAN               @"boolean"


#pragma mark - router

// 实际路由， 只有视图路由。
#define TEST_ROUTER_HOME                 @"axe://ios/test"
// 声明路由 ， 只有视图路由。
#define TEST_ROUTER_HOME_STATEMENT       @"axes://ios/test"
// 首页tab栏路由， 这是一个跳转路由。 我们约定将这个模块放在首页的tab栏上，使用改路由以跳转回该页面。
#define TEST_TABROUTER_HOME              @"axe://home/ios"
#define TEST_TABROUTER_PATH              @"ios"


#pragma mark - event

// 这里模拟了一个 退出登录操作。 所以我们可以发现， 这个通知即出现在 login中，又出现在 test模块中。
// 所以， 对于多模块共同操作的 通知和数据，还是应该放到 公共业务模块中。
#define TEST_EVENT_LOGINSTATUS           @"LoginStatusChange"

// 测试通知 eventA
#define TEST_EVENT_EVENTA                @"eventA"





