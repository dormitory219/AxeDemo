# demo-ground

demo app ground , grouping basic components 

## Ground

地基组件 :  整合基础组件和公共业务组件，打包成一个动态库（业务组件一般打包成静态库）， 供业务组件使用， 使所有业务组件可以依赖同一套基础内容， 同时由于是动态库，也保证了业务组件的单独编译。


## 内容

#### 公共基础组件

业务模块共用的基础组件，通过`Ground`打包到一起， 如示例中的 ：

    ss.dependency             "Axe"
    #需要引入所有依赖
    ss.dependency             "MXReact/CxxBridge"
    ss.dependency             "MXReact/RCTText"
    ss.dependency             "MXReact/RCTNetwork"
    ss.dependency             "MXReact/RCTImage"
    # 第三方组件示例
    ss.dependency             "Bugly"
    ss.dependency             'MBProgressHUD', '~> 1.1.0'
    ss.dependency             'Masonry'
    ss.dependency             'JSONModel'

在 [fastlane](https://github.com/axe-org/fastlane) 中，我们编写了处理头文件的脚本，使所有依赖的头文件，都能以原有的方式进行引用，详情可查看示例代码。

共用的基础组件放到这里，对于一些非共用的基础组件，依旧还是放到APP主工程中，如`demo-app`中的 `Bugly `.

#### 公共业务

公共业务指 公共的业务模块公用的一些业务逻辑， 当前项目中的代码就属于公共业务范畴 ：

* 对`ViewController`做了一些定制。
* 提供基础的`Model`基类，并指明使用JSONModel
* 为`WebView`容器提供一些扩展插件
* 为`ReactNative`容器提供一些扩展插件
* 应用的初始化设定，以及设置使用 `AXETabBarController`

以上是我们`Demo`中所做的内容，公共业务实际应该包含以下范围的内容：

* 基础组件的定制与扩展
* 公共业务的定义 （如公共的数据存储与事件通知）
* APP的初始化

我们称之为公共业务模块，这些内容属于`APP`的业务范畴，即`Ground`项目是针对一个APP的。基础组件是多APP共享的，而业务组件是一个APP所有的（但是定制一下，还是很容易迁移到其他APP上的）。

## 依赖编写

`Ground`项目的依赖，暂时不通过`AxeFile`管理。 所以需要手动编写[axe/Ground.podspec.rb](axe/Ground.podspec.rb) 和 [Podfile](Podfile) 两个文件

## 最后注意

使用私有`podspec`时， 我们必须要关闭 其自带的提交检测， 以避免提交时的编译校验。

提交进行编译校验是有必要的， 但是应该是更加严格的校验，对于我们的axe系统来说，我们要就的是进行自动化测试， 真机和模拟器测试。 而`CocoaPods`自带的编译和测试功能做不到我们的需求， 反而每次的自动检测会导致时间浪费，也很容易因为各种原因导致编译失败。 如常见的 第三方SDK的静态库或者指令集导致的问题。

所以，我们要关闭检测。

在 `/Library/Ruby/Gems/2.3.0/gems/cocoapods-1.5.0/lib/cocoapods/command/repo/push.rb` 文件中 `run`函数中注释`validate_podspec_files`：

	def run
          open_editor if @commit_message && @message.nil?
          check_if_master_repo
          #validate_podspec_files
          check_repo_status
          update_repo
          add_specs_to_repo
          push_repo unless @local_only
        end