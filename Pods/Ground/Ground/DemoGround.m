//
//  AXEGround.m
//  Ground
//
//  Created by 罗贤明 on 2018/3/10.
//  Copyright © 2018年 罗贤明. All rights reserved.
//
#import "DemoGround.h"
#import "Axe.h"
#import "AXEWKWebViewController.h"
#import "AXEReactViewController.h"
#import "OPOfflineManager.h"
#import "AXEDynamicRouter.h"
#import "AXEOfflineWKWebViewController.h"
#import "AXEOfflineReactViewController.h"
#import "AXEWebViewController.h"
#import "AXEOfflineWebViewController.h"
#import "AXETabBarController.h"
#import <React/RCTBridgeModule.h>


#import "AXEViewController+Demo.h"
#import "AXEWKWebViewController+Demo.h"
#import "NavigationController.h"





@interface DemoGround () <AXEModuleInitializer>
@end

@implementation DemoGround

AXEMODULEINITIALIZE_REGISTER()

- (void)AEXInitialize {
    [AXEAutoreleaseEvent registerSyncListenerForEventName:AXEEventModulesBeginInitializing handler:^(AXEData *payload) {
        // 添加 WKWebview和React的默认路由
        [[OPOfflineManager sharedManager] setUpWithPublicPKCS8Pem:@"-----BEGIN PUBLIC KEY-----\r\nMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEA4YXOMN8CxfZqDy2lpV+kbUgE4knWCG4k0M5/+lzOoEWl9eoohXw0Ln3dY0Cjx2EGsVCR5KzZVIfjRCiyQwdd8QYpmXwkXwbSq4hLtRPMN/411WN/zTgycaDEXlgqz5YZ3RReQzdzqj/KkLvwjFvaW6Q57CeEM52VaRhtYzMIU0WJuUwhsDKODg8jYzAOp3n+gKdUToOGiC/wG9HyU/0qt37gA/eHgRjOUcNJ1KT085+ddTGKHyopN+cTtNQ0nq+nzj5ZhF3Zl6iQ92JWSV9ERE62CvX+dPnyVWjOc/1jmcDgcaejJldFGLc2DjRMn148LM93kLDeCw35vhZTQeS+AwIDAQAB-----END PUBLIC KEY-----" baseURL:@"https://offline.demo.axe-org.cn/app/"];
        [AXEWKWebViewController setupCustom];
        [AXEWKWebViewController registerWKWebViewForHTTP];
        [AXEWKWebViewController registerWKWebViewForHTTPS];
        [AXEOfflineWKWebViewController registerWKWebVIewForOfflineHtml];
//        [AXEOfflineWebViewController registerUIWebVIewForOfflineHtml];
        [AXEOfflineReactViewController registerOfflineReactProtocol];
        [AXEReactViewController registerReactProtocol];
        [AXEReactViewController registerReactsProtocol];
        
    } priority:DEMOGROUND_MODULE_INIT_PRIORITY + 1];

    [AXEAutoreleaseEvent registerSyncListenerForEventName:AXEEventModulesBeginInitializing handler:^(AXEData *payload) {
        self.window = [[UIWindow alloc] initWithFrame:[UIScreen mainScreen].bounds];
        [AXETabBarController setNavigationControllerClass:[NavigationController class]];
        AXETabBarController *rootViewController = [AXETabBarController sharedTabBarController];
        self.window.rootViewController = rootViewController;
        [self.window makeKeyAndVisible];
    } priority:DEMOGROUND_MODULE_INIT_PRIORITY - 1];
    // 对于其他不重要的初始化， 建议以异步形式执行。
}



@end
