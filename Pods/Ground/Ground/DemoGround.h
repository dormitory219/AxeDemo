//
//  AXEGround.h
//  Ground
//
//  Created by 罗贤明 on 2018/3/10.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "AXEModelTypeData.h"



/**
  模块初始化所使用的优先级。
  业务模块在这个优先级进行同步初始化， 因为demo中使用了axeTabBarController ,所以必须明确业务模块初始化的时间，以保证tabbarController能正常初始化.
 
 */
#define DEMOGROUND_MODULE_INIT_PRIORITY  1000

typedef enum : NSUInteger {
    APPEnviromentSIT,
    APPEnviromentST,
    APPEnviromentPRD
} APPEnviroment;

/**
  一些基础的设定和初始化.
 */
@interface DemoGround : NSObject


@property (nonatomic,assign) APPEnviroment enviroment;

@property (strong, nonatomic) UIWindow *window;

@end
