//
//  AXEWKWebViewController+Demo.m
//  Ground
//
//  Created by 罗贤明 on 2018/5/18.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "AXEWKWebViewController+Demo.h"
#import "WKWebViewJavascriptBridge.h"
@implementation AXEWKWebViewController (Demo)

+ (void)setupCustom {
    // 进行一些自定义的修改
    [AXEWKWebViewController setCustomViewDidLoadBlock:^(AXEWKWebViewController *vc) {
        @weakify(vc)
        // 添加一些基础的原生方法，供js调用
        // 标题设置
        [vc.javascriptBridge registerHandler:@"set_title" handler:^(id data, WVJBResponseCallback responseCallback) {
            NSParameterAssert([data isKindOfClass:[NSString class]]);
            @strongify(vc)
            vc.navigationItem.title = data;
        }];
        // 页面关闭
        [vc.javascriptBridge registerHandler:@"close_page" handler:^(id data, WVJBResponseCallback responseCallback) {
            @strongify(vc)
            [vc.navigationController popViewControllerAnimated:YES];
        }];
        
        // 单页面应用时，返回键处理。
        if (vc.navigationController && vc.navigationController.viewControllers.count != 1) {
            // 不是 rootViewController时， 设置一个返回按钮。
            UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:vc action:@selector(goBack)];
            [vc.navigationItem setLeftBarButtonItem:backItem];
        }
        
    }];
}

// 返回， 但是要先检测webview状态
- (void)goBack {
    if (self.webView.canGoBack) {
        [self.webView goBack];
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}
@end
