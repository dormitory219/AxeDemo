//
//  DemoImagePickModule.h
//  Ground
//
//  Created by 罗贤明 on 2018/5/18.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <React/RCTBridgeModule.h>

/**
 做一个选择图片的插件。
 原因是 使用 ImagePickerIOS ImageStore ImageEditor 这些鬼东西，又难用，还不知道图片类型，直接返回一个base64字符串。
 我们要兼容 js容器，就必须指明图片类型，即使用 url形式 ：
 */
@interface DemoImagePickModule : NSObject <RCTBridgeModule>

@end
