//
//  BasicModel.h
//  Ground
//
//  Created by 罗贤明 on 2018/5/18.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "JSONModel.h"
#import "AXEModelTypeData.h"

/**
 model类型基类 ,业务组件的model类型都以 BasicModel为基类。
 */
@interface BasicModel : JSONModel <AXEDataModelProtocol>

@end
