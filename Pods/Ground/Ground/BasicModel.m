//
//  BasicModel.m
//  Ground
//
//  Created by 罗贤明 on 2018/5/18.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "BasicModel.h"

@implementation BasicModel
- (void)axe_modelSetWithJSON:(NSDictionary *)json {
    NSParameterAssert([json isKindOfClass:[NSDictionary class]]);
    NSError *error;
    [self mergeFromDictionary:json useKeyMapping:YES error:&error];
    if (error) {
        NSLog(@"mergeFromDictionary 失败 : %@",error);
    }
    // 由于jsonModel 没有 根据dic设置model的方法，所以只能用这个 merge方法， 对于空值还要特殊处理一下。
    [json enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, id  _Nonnull obj, BOOL * _Nonnull stop) {
        if ([obj isKindOfClass:[NSNull class]]) {
            [self setValue:nil forKey:key];
        }
    }];
}

- (NSDictionary *)axe_modelToJSONObject {
    return [self toDictionary];
}

@end
