//
//  NavigationController.m
//  Ground
//
//  Created by 罗贤明 on 2018/5/18.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "NavigationController.h"

@implementation NavigationController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationBar.backgroundColor = [UIColor whiteColor];
}

@end
