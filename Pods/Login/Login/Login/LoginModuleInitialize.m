//
//  LoginModuleInitialize.m
//  Login
//
//  Created by 罗贤明 on 2018/4/15.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "LoginModuleInitialize.h"
#import "DemoGround.h"
#import "LoginViewController.h"
#import "RegisterViewController.h"

@implementation LoginModuleInitialize

AXEMODULEINITIALIZE_REGISTER()

- (void)AEXInitialize {
    [AXEAutoreleaseEvent registerSyncListenerForEventName:AXEEventModulesBeginInitializing handler:^(AXEData *payload) {
        [[AXERouter sharedRouter] registerPath:@"login/login" withJumpRoute:^(AXERouteRequest *request) {
            LoginViewController *vc = [[LoginViewController alloc] init];
            vc.routeRequest = request;
            vc.hidesBottomBarWhenPushed = YES;
            // TODO 注明 fromVC必须使用 子viewcontroller，不能是 uiNavigationController.
            [request.fromVC.navigationController pushViewController:vc animated:YES];
        }];
        [[AXERouter sharedRouter] registerPath:@"login/register" withJumpRoute:^(AXERouteRequest *request) {
            RegisterViewController *vc = [[RegisterViewController alloc] init];
            vc.routeRequest = request;
            vc.hidesBottomBarWhenPushed = YES;
            [request.fromVC.navigationController pushViewController:vc animated:YES];
        }];
    } priority:DEMOGROUND_MODULE_INIT_PRIORITY];
    
}

@end
