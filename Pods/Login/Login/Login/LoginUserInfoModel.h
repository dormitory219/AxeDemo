//
//  LoginUserInfoModel.h
//  Login
//
//  Created by 罗贤明 on 2018/5/18.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BasicModel.h"


/**
  登录信息， 由后台返回的model类型， 可以提供给 JS模块使用。
 */
@interface LoginUserInfoModel : BasicModel

@property (nonatomic,strong) NSString *account;
@property (nonatomic,strong) NSNumber *level;
@property (nonatomic,strong) NSDictionary *detailInfo;
@property (nonatomic,strong) NSArray *tagList;

@end
