//
//  LoginViewController.m
//  Login
//
//  Created by 罗贤明 on 2018/4/15.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import "LoginViewController.h"
#define MAS_SHORTHAND_GLOBALS
#import "Masonry.h"
#import <QuartzCore/QuartzCore.h>
#import "MBProgressHUD.h"
#import "DemoGround.h"
#import "LoginUserInfoModel.h"
#import "RegisterViewController.h"
#import "API.h"

@interface LoginViewController ()

@property (nonatomic,strong) UITextField *accountTextField;

@property (nonatomic,strong) UITextField *passowrdTextField;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.navigationItem.title = @"登录";
    
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissKeyboard)];
    [self.view addGestureRecognizer:tap];
    
    UIView *container = [[UIView alloc] init];
    [self.view addSubview:container];
    [container mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.centerY.equalTo(self.view);
        make.height.equalTo(160);
    }];
    
    UILabel *accountLabel = [[UILabel alloc] init];
    accountLabel.text = @"帐号";
    accountLabel.translatesAutoresizingMaskIntoConstraints = NO;
    accountLabel.textAlignment = NSTextAlignmentCenter;
    accountLabel.font = [UIFont systemFontOfSize:16];
    [container addSubview:accountLabel];
    [accountLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(container);
        make.left.equalTo(container).offset(20);
        make.height.equalTo(36);
        make.width.equalTo(50);
    }];
    
    UITextField *accountInput = [[UITextField alloc] init];
    accountInput.clearButtonMode = UITextFieldViewModeWhileEditing;
    accountInput.translatesAutoresizingMaskIntoConstraints = NO;
    accountInput.font = [UIFont systemFontOfSize:14];
    accountInput.layer.cornerRadius = 3;
    accountInput.layer.masksToBounds = YES;
    accountInput.layer.borderColor = [UIColor colorWithWhite:0 alpha:0.3].CGColor;
    accountInput.layer.borderWidth = 0.5;
    [container addSubview:accountInput];
    [accountInput mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(accountLabel);
        make.right.equalTo(container).offset(-10);
        make.width.equalTo(200);
    }];
    _accountTextField = accountInput;
    
    UILabel *passowrdLabel = [[UILabel alloc] init];
    passowrdLabel.text = @"密码";
    passowrdLabel.translatesAutoresizingMaskIntoConstraints = NO;
    passowrdLabel.textAlignment = NSTextAlignmentCenter;
    passowrdLabel.font = [UIFont systemFontOfSize:16];
    [container addSubview:passowrdLabel];
    [passowrdLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(accountInput.mas_bottom).offset(12);
        make.left.equalTo(container).offset(20);
        make.height.equalTo(36);
        make.width.equalTo(50);
    }];
    
    UITextField *passowrdInput = [[UITextField alloc] init];
    passowrdInput.translatesAutoresizingMaskIntoConstraints = NO;
    passowrdInput.clearButtonMode = UITextFieldViewModeWhileEditing;
    passowrdInput.font = [UIFont systemFontOfSize:14];
    passowrdInput.secureTextEntry = YES;
    passowrdInput.layer.cornerRadius = 3;
    passowrdInput.layer.masksToBounds = YES;
    passowrdInput.layer.borderColor = [UIColor colorWithWhite:0 alpha:0.3].CGColor;
    passowrdInput.layer.borderWidth = 0.5;
    [container addSubview:passowrdInput];
    [passowrdInput mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.equalTo(passowrdLabel);
        make.right.equalTo(container).offset(-10);
        make.width.equalTo(200);
    }];
    _passowrdTextField = passowrdInput;
    
    
    UIButton *loginButton = [UIButton buttonWithType:UIButtonTypeCustom];
    loginButton.translatesAutoresizingMaskIntoConstraints = NO;
    [loginButton setTitle:@"登录" forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [loginButton setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6] forState:UIControlStateHighlighted];
    [loginButton setBackgroundColor:[UIColor colorWithRed:72./255 green:143./255 blue:240./255 alpha:1]];
    loginButton.layer.masksToBounds = YES;
    loginButton.layer.cornerRadius = 4;
    loginButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    loginButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [loginButton addTarget:self action:@selector(logIn) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:loginButton];
    [loginButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.equalTo(passowrdInput.mas_bottom).offset(20);
        make.left.equalTo(container).offset(20);
        make.width.equalTo(120);
        make.height.equalTo(44);
    }];
    
    UIButton *registerButton = [UIButton buttonWithType:UIButtonTypeCustom];
    registerButton.translatesAutoresizingMaskIntoConstraints = NO;
    [registerButton setTitle:@"注册" forState:UIControlStateNormal];
    [registerButton setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [registerButton setTitleColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.6] forState:UIControlStateHighlighted];
    [registerButton setBackgroundColor:[UIColor colorWithRed:72./255 green:143./255 blue:240./255 alpha:1]];
    registerButton.layer.masksToBounds = YES;
    registerButton.layer.cornerRadius = 4;
    registerButton.titleLabel.textAlignment = NSTextAlignmentCenter;
    registerButton.titleLabel.font = [UIFont boldSystemFontOfSize:16];
    [registerButton addTarget:self action:@selector(jumpToRegister) forControlEvents:UIControlEventTouchUpInside];
    [container addSubview:registerButton];
    [registerButton mas_makeConstraints:^(MASConstraintMaker *make) {
        make.top.bottom.width.equalTo(loginButton);
        make.right.equalTo(self.view).offset(-20);
    }];
    // 处理传递参数。
    NSString *account = [self.routeRequest.params stringForKey:LOGIN_DATA_ACCOUNT];
    if (account) {
        _accountTextField.text = account;
    }
}

- (void)logIn {
    NSString *account = _accountTextField.text;
    if ([account isEqualToString:@""]) {
        [self toastMessage:@"请输入帐号！！"];
        return;
    }
    NSString *passowrd = _passowrdTextField.text;
    if ([passowrd isEqualToString:@""]) {
        [self toastMessage:@"请输入密码！！"];
        return;
    }
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeIndeterminate;
    hud.label.text = @"登录中。。。";
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        hud.mode = MBProgressHUDModeText;
        hud.label.text = @"登录成功。。。";
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
            AXEData *data = [AXEData dataForTransmission];
            LoginUserInfoModel *userInfo = [[LoginUserInfoModel alloc] init];
            userInfo.account = account;
            userInfo.level = @11;
            userInfo.detailInfo = @{
                                    @"firstName": @"hello",
                                    @"lastName": @"world",
                                    @"gender": @"FTM"
                                    };
            userInfo.tagList = @[@"a",@"b",@"c"];
            
            [data setData:userInfo forKey:LOGIN_DATA_USERINFO];
            [[AXEData sharedData] setData:userInfo forKey:LOGIN_DATA_USERINFO];
            [data setBool:YES forKey:LOGIN_DATA_LOGIN];
            // TODO data需要copy一份。。。
            [AXEEvent postEventName:LOGIN_EVENT_LOGINSTATUS withPayload:data];
            if (self.routeRequest.callback) {
                // 如果有回调。
                self.routeRequest.callback(data);
            }
            // 关闭页面。
            [self.navigationController popToViewController:self.routeRequest.fromVC animated:YES];
        });
    });
}

- (void)jumpToRegister {
    RegisterViewController *vc = [[RegisterViewController alloc] init];
    // 这里是我们传递回调给 registerViewController . 因为我们有这个需求，所以才会传递，以在注册成功的时候，如登录成功一样进行回调。
    vc.routeRequest = self.routeRequest;
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)toastMessage:(NSString *)massage {
    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    hud.mode = MBProgressHUDModeText;
    hud.detailsLabel.text = massage;
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(2.5 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [hud hideAnimated:YES];
    });
}

- (void)dismissKeyboard {
    [_passowrdTextField resignFirstResponder];
    [_accountTextField resignFirstResponder];
}


@end
