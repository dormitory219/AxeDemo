//
//  LoginModuleInitialize.h
//  Login
//
//  Created by 罗贤明 on 2018/4/15.
//  Copyright © 2018年 罗贤明. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Axe.h"

/**
  模块初始化
 */
@interface LoginModuleInitialize : NSObject <AXEModuleInitializer>

@end
